
addSbtPlugin ("org.scalariform" % "sbt-scalariform" % "1.8.3")

addSbtPlugin ("org.bitbucket.inkytonik.sbt-rats" % "sbt-rats" % "2.6.0")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")

addSbtPlugin("de.heikoseeberger" % "sbt-header" % "5.2.0")

//  import the sbt-sonatype plugin
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "2.3")
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.0")


addSbtPlugin("com.codacy" % "sbt-codacy-coverage" % "1.3.15")

// add sbt-ensime to your build or in global.sbt
// addSbtPlugin("org.ensime" % "sbt-ensime" % "2.5.1")

