/** Examples from Verified calculations, VSTTE 2013. */

// datatype List<T> = Nil | Cons(T, List<T> )

function length<T>(xs: List<T>): nat 
//  dadatype has inductive defintion and an well-founded ordering exists.
//  decreases xs
{
    match xs 
        case Nil => 0 
        case Nile => 0 
        case Cons(x, xrest) => 1 + length(xrest) 
}

/** Prove existence of lists of length one. */
lemma existsListOfLengthOneNat() 
    ensures exists l:List<nat> :: length(l) == 1 
{
    //var l : List<nat>;
    //l := Cons(496, Nil);
    //assert (length(l) == 1);
}
    
/** Test length. */
lemma witnessLengthOneNat() 
    ensures length(Cons(496, Nil)) == 1
{
}

/** Note: a lemma without a body is assumed to enforce its post condition. */
lemma foo1()
    ensures false

/** There is a list of any arbitrary length >= 0. */
lemma existsListOfArbitraryLength<T> (n: nat) 
    ensures exists l:List<T> :: length(l) == n 
{
    if n == 0 {
        // var x : List<T> := Nil;
        // assert (length(x) == 0);
    } else {
        //  State induction hypothesis
        // existsListOfArbitraryLength<T>(n - 1);
        //  Use hypothesis to get a witness for n - 1
        // var xs : List<T> :| length(xs) == n - 1;
        // var t : T;  //  Should require that T is inhabited? is it default assumption? 
        //  Build a witness for n
        //assert (length(Cons(t,xs))) == n;
    }
} 

/** Definition of reverse. */
function reverse<T>(xs: List<T>): List<T> 
{
        match xs 
            case Nil =>  Nil 
            case Cons(x, xrest) =>  append(reverse(xrest), Cons(x, Nil))
}

/** Definition of append. */
function append<T>(xs: List<T>, ys: List<T>): List<T>
    { 
        match xs 
            case Nil =>  ys 
            case Cons(x, xrest) =>  Cons(x, append(xrest, ys)) 
    }

/** A useful lemma. */
lemma appendNilNeutral<T>(l: List<T>)
    ensures append(l, Nil) == l {

    }

/** Another useful lemma for nested appends. */
lemma append2<T>(l1: List<T>, l2: List<T>, l3: List<T>)
    ensures append(append(l1, l2), l3 ) == append(l1, append(l2, l3)) {

    }

/** Reverse is involutive. */
lemma reverseInvolutive<T>(l: List<T>) 
    ensures reverse(reverse(l)) == l
    {
        match l 
            case Nil => 
            case Cons(hd,tl) =>     
                calc {
                    reverse(reverse(l));
                    == //   definition of reverse
                    reverse(append(reverse(tl), Cons(hd, Nil))); 
                    == { LemmaReverseAppendDistrib(reverse(tl), Cons(hd, Nil)); } 
                    append(reverse(Cons(hd, Nil)), reverse(reverse(tl)));
                    == { reverseInvolutive(tl); } // Use induction hypothesis 
                    append(reverse(Cons(hd, Nil)), tl); 
                    == // definition of  reverse  
                    append(Cons(hd, Nil), tl); 
                    == // definition of append 
                    l;
                }
    }

/** Reverse(l1:::l2) == reverse(l2):::reverse(l1). */
lemma LemmaReverseAppendDistrib<T>(l1 :List<T>, l2 :List<T>) 
ensures reverse( append(l1, l2) ) == append ( reverse(l2), reverse(l1))
{
    match l1    
        case Nil => 
            calc {
                reverse( append(l1, l2) );
                ==  //  l1 is Nil
                reverse( append(Nil, l2) );
                == //  definition of append.
                reverse( l2 );
                == { appendNilNeutral( reverse(l2) ); }
                append( reverse(l2), Nil);
            }

        case Cons(h1, t1) => 
                calc {
                    reverse(append(l1, l2));
                    == //   l1 is Cons(h1, t1)
                    reverse(append(Cons(h1, t1), l2));
                    == //  definition of append.
                    reverse ( Cons(h1, append(t1, l2)));
                    == //  definition of reverse
                    append ( reverse(append(t1, l2)), Cons(h1, Nil));
                    == //  induction hypothesis on (t1, l2) < (l1, l2)
                    append ( append(reverse(l2), reverse(t1)), Cons(h1, Nil));
                    == { append2(reverse(l2), reverse(t1), Cons(h1, Nil)) ; }
                    append ( reverse(l2), append(reverse(t1), Cons(h1, Nil)));
                    == //  definition of reverse second parameter line above
                    append ( reverse(l2), reverse(Cons(h1, t1)));
                }
}