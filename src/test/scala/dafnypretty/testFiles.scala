package org.bitbucket.franck44.dafnypretty
package tests

import DafnySyntax.{ ASTNode, DafnyProgram }
import org.bitbucket.inkytonik.kiama.util.TestCompilerWithConfig
import org.scalatest._

import org.bitbucket.inkytonik.kiama.util.{ FileSource, StringSource }

class ParseFileTests extends FunSuite with Matchers with Driver {

    val pathToFiles = "testFiles"

    import scala.io.Source
    import scala.util.{ Try, Success, Failure }

    override val logger = getLogger( this.getClass() )

    /**
     * Read file from resources/testFiles
     *
     * @param filename  The file to be read.
     */
    def testFile( filename : String ) {
        //  getLines removes newlines so we add them in mkString
        val r = parse( StringSource(
            Source.fromResource ( "testFiles/" + filename + ".dafny" )
                .getLines
                .mkString( "\n" ) ) )
        r match {
            case Success( v ) ⇒ {
                logger.info( s"Parsing file $filename succeeded" )
                succeed
            }
            case Failure( f ) ⇒
                logger.error( s"Parsing file $filename failed" )
                fail( f )
        }
    }

    test( "Parsing File ex2" ) {
        testFile( "ex2" )
    }

    test( "Parsing File ex3" ) {
        testFile( "ex3" )
    }

    test( "Parsing File ex4" ) {
        testFile( "ex4" )
    }

    test( "Parsing File ex5" ) {
        testFile( "ex5" )
    }

    test( "Parsing File ex6" ) {
        testFile( "ex6" )
    }
}