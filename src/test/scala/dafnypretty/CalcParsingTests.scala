package org.bitbucket.franck44.dafnypretty
package tests

import DafnySyntax._
import org.bitbucket.inkytonik.kiama.util.TestCompilerWithConfig
import org.scalatest._

import org.bitbucket.inkytonik.kiama.util.{ FileSource, StringSource }
import Implicits._

class CalcParsingTests extends FunSuite with Matchers with Formatter {

    import scala.io.Source
    import scala.util.{ Try, Success, Failure }

    override val logger = getLogger( this.getClass() )

    val parseCalc = DafnyParser[ TestCalc ]
    val parseCaseExpr = DafnyParser[ TestCaseExpression ]
    val parseExpression = DafnyParser[ TestExpression ]
    val parseTerm = DafnyParser[ TestTerm ]
    val parseEndLessExpr = DafnyParser[ TestEndlessExpr ]
    val parseMatchStmt = DafnyParser[ TestMatchStmt ]
    val parseCaseStmt = DafnyParser[ TestCaseStmt ]
    val parseMethodDecl = DafnyParser[ TestMethodDecl ]

    ignore( "Parse Calc statement" ) {
        val res = parseCalc( "calc { l ; == l2; <= { f(x) ; } l3; }" )
        res match {
            case Success( TestCalc( v ) ) ⇒
                logger.info( prettyProduct( v ) )

            case Failure( f ) ⇒ logger.error( f.getMessage )
        }
    }

    val test1 = """ |   
                    |case Nil => 
                    |           calc{}
                    |""".stripMargin

    ignore( "Parse more Calc statement in case Expression" ) {
        val res = parseCaseExpr( test1 )
        println( res )
        res match {
            case Success( TestCaseExpression( v ) ) ⇒
                logger.info( prettyProduct( v ) )

            case Failure( f ) ⇒ logger.error( f.getMessage )
        }
        // logger.info( prettyProduct( res ) )
        // res shouldBe Success( Calc( CalcBody( List() ) ) )

    }

    val test2 = """ |   
                    |       
                    |calc {}
                    |""".stripMargin

    ignore( "Parse Calc statement as Term" ) {
        val res = parseEndLessExpr( test2 )
        res match {
            case Success( TestEndlessExpr( v ) ) ⇒
                logger.info( prettyProduct( v ) )

            case Failure( f ) ⇒ logger.error( f.getMessage )
        }
        // logger.info( prettyProduct( res ) )
        // res shouldBe Success( Calc( CalcBody( List() ) ) )

    }

    val test3 = """ |match x
                    |case Nil => 
                    | 
                    |   case Cons(hd,tl) =>     
                    |    calc {
                    |        reverse(reverse(l));
                    |        == //   definition of reverse
                    |        l;
                    |    }
                    |   
                    |""".stripMargin

    ignore( "Parse match statement as Term" ) {
        val res = parseMatchStmt( test3 )
        res match {
            case Success( TestMatchStmt( v ) ) ⇒
                logger.info( prettyProduct( v ) )

            case Failure( f ) ⇒ logger.error( f.getMessage )
        }
        // logger.info( prettyProduct( res ) )
        // res shouldBe Success( Calc( CalcBody( List() ) ) )

    }

    val test4 = """|lemma existsListOfLengthOneNat() 
                   |   ensures exists l:List<nat> :: length(l) == 1 
                   | {
                   |}
                   }
                   |""".stripMargin

    test( "Parse lemma as MethodDecl" ) {
        val res = parseMethodDecl( test4 )
        res match {
            case Success( TestMethodDecl( v ) ) ⇒
                logger.info( prettyProduct( v ) )

            case Failure( f ) ⇒ logger.error( f.getMessage )
        }
        // logger.info( prettyProduct( res ) )
        // res shouldBe Success( Calc( CalcBody( List() ) ) )

    }
}

