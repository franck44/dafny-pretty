/*
 * This file is part of DafnyPP.
 *
 * Copyright (C) 2020 Franck Cassez.
 *
 * DafnyPP is free software: you can  redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * DafnyPP is distributed in the hope that it will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DafnyPP.  (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.dafnypretty

import DafnySyntax._
import org.bitbucket.inkytonik.kiama.util.{
    CompilerBase,
    Config
}
import configurations.{ DafnyPPConfig, DefaultConfig }
import org.bitbucket.franck44.dafnypretty.configurations.DefaultConfig
import com.twitter.util.Config.Required

trait Formatter extends Driver {

    import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.{ Document, emptyDocument }
    import org.bitbucket.franck44.dafnypretty.DafnyPrettyPrinter.{ show ⇒ origShow }
    import org.bitbucket.inkytonik.kiama.util.StringOps.lines

    import org.bitbucket.inkytonik.kiama.util.{ Config, Source }
    import org.bitbucket.inkytonik.kiama.util.Messaging.Messages
    import scala.util.{ Try, Success, Failure }

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    override val logger = getLogger( this.getClass() )

    def myPretty( t : ASTNode ) : String =
        super.pretty ( show ( t ) ).layout

    def prettyProduct[ T <: Product ]( t : T ) : String =
        super.pretty( any( t ) ).layout

    /**
     * Parenthesised group.
     *
     * @param  d   The doc to be parenthesised.
     * @return     A document that encloses a given document between left
     *              and right parenthesis and some inside spaces.
     */
    override def parens( d : Doc ) : Doc =
        enclose(
            lparen <> spaces( spacesInsideParens ),
            d,
            spaces( spacesInsideParens ) <> rparen )

    // need fully qualified Seq as Kiama has 2.13 types for Seq.
    //  see @link{https://docs.scala-lang.org/overviews/core/collections-migration-213.html}
    def mlsep( ds : scala.collection.immutable.Seq[ Doc ], sep : Doc ) : Doc =
        if ( ds.isEmpty )
            emptyDoc
        else
            linebreak <> folddoc( ds, _ <> sep <> spaces( spacesAfterComma ) <> _ )

    // def myfillsep( ds : scala.collection.immutable.Seq[ Doc ], sep : Doc ) : Doc =
    //     folddoc( ds, ( _ <> sep <> ( </> <> spaces( 2 ) ) <> _ ) )

    def eolsToDoc( s : Int ) =
        if ( s <= 0 )
            emptyDoc
        else
            // hcat( Range( 0, s ).map( { x : Int ⇒ value( x ) <> line } ) )
            hcat( Range( 0, s ).map( _ ⇒ line ) )

    def origShow2( t : ASTNode ) : Doc = origShow( t, defaultWidth )

    def sortRequiresFirst( x : List[ Spec ] ) = x
    /**
     * Pretty print a Dafny program.
     *
     * @param t    A Dafny non-terminal node.
     * @return     A document following formatting rules for `t`.
     */
    def show( t : ASTNode ) : Doc =
        t match {

            case DafnyProgram( includes, eols, topDecls ) ⇒
                vcat ( includes.map( show ) ) <@@> line <> vsep( topDecls.map( show ), line )

            // case IncludeDirective(stringToken, eols) =>
            //     "include"

            // case EndOfLines( optEndOfLines ) ⇒
            //     hcat( optEndOfLines.map( _ ⇒ line ) )

            //  Use ssep to concatenate the doc without any additional separator.
            // case TopDecl( blocks ) ⇒
            //     vsep( blocks.map( show ), line )

            // case FunctionDecl( functionDecl ) ⇒ show( functionDecl )

            case SLCommentEOL( txt ) ⇒
                show( txt )

            case MLComment( txt ) ⇒
                /*
                 * This rule is a bit tricky. It inserts a * before each line
                 * in the comment and prints it out.
                 */
                "/*" <> nest(
                    line <> "* " <> ssep(
                        txt.split( '\n' ).toList.map( string ),
                        line <> "* " ) <@@>
                        "*/", 1 )

            case MLDocCommentExpr( mLComment ) ⇒
                show( mLComment )

            // case MLComment( mLComment ) ⇒
            //     value( mLComment )

            case MLDocComment( txt ) ⇒
                /*
                 * This rule is a bit tricky. It inserts a * before each line
                 * and prints it out.
                 */
                "/**" <> nest(
                    line <> "*" <+> ssep(
                        txt.split( "\n" ).toList.map( string ),
                        line <> "*" <+> emptyDoc ) <@@> "*/", 1 )

            case RequiresClause( expr ) ⇒ "requires" <+> show( expr )
            // case FunctionEnsuresClause( expr ) ⇒ "ensures" <+> show( expr )

            case FDoc( d )              ⇒ show( d ) <> line

            case NoDoc()                ⇒ emptyDoc

            case FunctionDecl( doc, functionHeading, funcSpec, functionBody ) ⇒
                show( doc ) <> show( functionHeading ) <+> nest ( show( funcSpec ) ) <@@> show( functionBody )

            case Formals( optPairVarTypes ) ⇒ arguments(
                optPairVarTypes,
                elemToDoc = show,
                sep       = comma,
                sepfn     = mlsep )

            case FunctionSignatureOrEllipsis( _, formals, Some( domainType ) ) ⇒
                show( formals ) <> ":" <+> origShow( domainType )

            case FunctionSignatureOrEllipsis( _, formals, None ) ⇒
                show( formals )

            case PairVarType( ident, domainType ) ⇒
                origShow( ident ) <> spaces( 1 ) <> ":" <> spaces( 1 ) <>
                    origShow( domainType )

            case FunBody( expressions ) ⇒
                // show( expression )
                //  Remove trailing EOLS in a function body
                val filteredExpr = expressions.reverse.dropWhile( { case x : EOLS ⇒ true; case _ ⇒ false } ).reverse
                "{" <> nest ( line <> ssep( filteredExpr.map( show ), emptyDoc ) ) <> line <> "}"

            case Expr( term ) ⇒
                show( term )

            case Term( factor, optNextFactors ) ⇒
                //Print an expressio nto fill the line and then ident following lines.
                hang ( fillcat( ( factor :: optNextFactors ).map( origShow2 ) ) )

            case Factor( unaryExpression, optNextUnaryExprs ) ⇒ show( unaryExpression ) <>
                ssep( optNextUnaryExprs.map( show ), " " )

            case PrimUnaryExpr( primaryExpression ) ⇒ show( primaryExpression )

            case FunctionHeading( tipe, name, signature ) ⇒
                show( tipe ) <> show( name ) <> show( signature )

            case FunSpec( xs ) ⇒ xs match {
                case Nil ⇒ emptyDoc
                case x ⇒
                    //  Sort requires first
                    ssep( sortRequiresFirst( x ).map( show ), line )
            }

            case CO( x, eols ) ⇒
                show( x ) <> eolsToDoc( eols.size - 1 )

            case FD( x, _ ) ⇒
                show( x )

            case EOLS( x ) if x.size <= 1 ⇒ eolsToDoc( x.size )

            case EOLS( x ) if x.size > 1 ⇒
                //  If EOLS should be collapsed, print only 2
                if ( collapseEOLS )
                    eolsToDoc( 2 )
                else
                    eolsToDoc( x.size )

            case x ⇒ origShow( x )
        }

    // def makeDoc(t : DafnyProgram) =
}

