/*
 * This file is part of DafnyPP.
 *
 * Copyright (C) 2020 Franck Cassez.
 *
 * DafnyPP is free software: you can  redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * DafnyPP is distributed in the hope that it will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DafnyPP.  (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.dafnypretty
package configurations

/**
 * A formatter configuration..
 *
 *
 */
trait DafnyPPConfig {
    /** The number of spaces after a comma. */
    val spacesAfterComma : Int

    /** The number of spaces inside a parenthesis.ed expression. */
    val spacesInsideParens : Int

    val collapseEOLS : Boolean
}

/**
 * A default formatter configuration.
 */
trait DefaultConfig extends DafnyPPConfig {

    val spacesAfterComma = 1

    val spacesInsideParens = 0

    val collapseEOLS = true

}

