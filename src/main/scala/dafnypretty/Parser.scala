/*
 * This file is part of DafnyPP.
 *
 * Copyright (C) 2020 Franck Cassez.
 *
 * DafnyPP is free software: you can  redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * DafnyPP is distributed in the hope that it will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DafnyPP.  (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.dafnypretty

import DafnySyntax._
import org.bitbucket.inkytonik.kiama.util.{
    CompilerBase,
    Config
}
import configurations.{ DafnyPPConfig, DefaultConfig }
import org.bitbucket.franck44.dafnypretty.configurations.DefaultConfig
import com.twitter.util.Config.Required

/**
 * Parser for Dafny programs.
 */
trait Driver
    extends CompilerBase[ ASTNode, DafnyProgram, Config ]
    with DefaultConfig
    with org.bitbucket.inkytonik.kiama.output.PrettyPrinter {

    import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.{ Document, emptyDocument }
    import org.bitbucket.franck44.dafnypretty.DafnyPrettyPrinter.{ show ⇒ origShow }
    import org.bitbucket.inkytonik.kiama.util.StringOps.lines

    import org.bitbucket.inkytonik.kiama.util.{ Config, Source }
    import org.bitbucket.inkytonik.kiama.util.Messaging.Messages
    import scala.util.{ Try, Success, Failure }

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    /**
     * A parser exception.
     */
    case class ParseErrorException( msg : String ) extends Exception( msg )

    val name = "dfy"

    /**
     * Configuration for parsing.
     *
     * @param  args    Parsing options.
     * @return         A configuration for the selected options.
     */
    def createConfig( args : Seq[ String ] ) : Config =
        new Config( args )

    /**
     *
     *
     * @param  source  The text to be parsed.
     * @param  config  The configuration for the parsing phase.
     * @return         An Dafny program AST or parse errors.
     */
    override def makeast( source : Source, config : Config ) : Either[ DafnyProgram, Messages ] = {
        val p = new Dafny( source, positions )
        val pr = p.pDafnyProgram( 0 )
        if ( pr.hasValue )
            Left( p.value( pr ).asInstanceOf[ DafnyProgram ] )
        else
            Right( Vector( p.errorToMessage( pr.parseError ) ) )
    }

    /**
     * Process a file according to some configuration.
     *
     * @param  source  The text to be processed.
     * @param  ast     The AST for the text.
     * @param config   The configuration.
     */
    def process( source : Source, ast : DafnyProgram, config : Config ) {
        // config.output().emitln (pretty (any (ast)).layout)
        config.output().emit( format( ast ).layout )
    }

    /**
     * Default pretty printer.
     *
     * @param      ast     A Dafny program.
     * @return             A formatted document.
     */
    def format( ast : DafnyProgram ) : Document =
        DafnyPrettyPrinter.format( ast, 5 )

    /**
     * Wrapper parser. Transforms the result of makeast into a Try.
     *
     * @param  s   The text to be parsed.
     * @return     A Dafny program AST or reasons for failure.
     */
    def parse( s : Source ) : Try[ DafnyProgram ] = {
        makeast( s, createConfig( List() ) ) match {
            case Left( x ) ⇒ Success( x )
            case Right( m ) ⇒
                //  m is a vector of messages
                Failure( new ParseErrorException( s"Parse error ${messaging.formatMessages( m )}" ) )
        }
    }
}

