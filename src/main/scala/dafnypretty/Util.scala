package org.bitbucket.franck44.dafnypretty

import org.bitbucket.inkytonik.kiama.util.{
    CompilerBase,
    Config
}
import DafnySyntax._

// import configurations.{ DafnyPPConfig, DefaultConfig }
// import org.bitbucket.franck44.dafnypretty.configurations.DefaultConfig
// import com.twitter.util.Config.Required

object Helpers {

    def countEOLs( s : String ) : Int = s.count( _ == '\n' )

}

/**
 * Generic parser.
 *
 * @tparam  T   The type (non-terminal) the parser should return.
 * @param   x   A map that associates a subparser to an Dafny object,
 *              Typically this is _ => _.pTerm or _ => _pCommand
 *
 */
class DafnyParser[ T <: ASTNode ]( x : Dafny ⇒ ( Int ⇒ xtc.parser.Result ) ) extends CompilerBase[ ASTNode, T, Config ] {

    import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
    import org.bitbucket.inkytonik.kiama.util.{ Emitter, OutputEmitter, Source, FileSource }
    import org.bitbucket.inkytonik.kiama.util.Messaging.Messages

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    val name = "dfy"

    def createConfig( args : Seq[ String ] ) : Config = new Config( args )

    //  build an AST from a Source if possible

    override def makeast( source : Source, config : Config ) : Either[ T, Messages ] = {
        logger.debug( "entering makeast" )
        val p = new Dafny( source, positions )

        logger.info( "getting a parser for {}", x( p )( 0 ) )

        // get the parser for x

        val pr = x( p )( 0 )
        if ( pr.hasValue )
            Left( p.value( pr ).asInstanceOf[ T ] )
        else
            Right( Vector( p.errorToMessage( pr.parseError ) ) )
    }

    //  not needed as only parsing is required

    def process( source : Source, ast : T, config : Config ) : Unit = {}

    def format( ast : T ) : Document =
        DafnyPrettyPrinter.format( ast, 5 )

}

/**
 * Parser factory.
 *
 * To get a parser for temrs of type T, use DafnyParser2[T].
 * `T` must be  strict subtype of ASTNode
 */
object DafnyParser {
    import DafnySyntax._
    import scala.reflect.ClassTag
    import scala.util.{ Failure, Success, Try }
    import org.bitbucket.inkytonik.kiama.util.Source

    /**
     * An parser exception
     */
    case class ParseErrorException( msg : String ) extends Exception( msg )

    /**
     * associate a parser of type T with ClassOf[T]
     *
     * @param mf This an implicit and there is no need to pass a parameter.
     *
     */
    def apply[ T <: ASTNode ]( implicit mf : ClassTag[ T ] ) : Source ⇒ Try[ T ] = {
        try {
            parser[ T ](
                dispatch.find(
                    { case ( clazz, _ ) ⇒ clazz isAssignableFrom mf.runtimeClass } ).map( { case ( _, parser ) ⇒ parser } ).get )
        } catch {
            case scala.util.control.NonFatal( e ) ⇒ throw ( new Exception( s"Type $mf not supported in object DafnyParser -- ${e.getMessage}" ) )
        }
    }

    //  the available parsers descriptions
    import xtc.parser.Result

    //  format: OFF
    private val dafnyProgramParser  : Dafny ⇒ ( Int ⇒ Result ) = _.pDafnyProgram
    private val calcParser          : Dafny ⇒ ( Int ⇒ Result ) = _.pTestCalc
    private val matchParser         : Dafny ⇒ ( Int ⇒ Result ) = _.pTestMatchExpression
    private val caseExprParser      : Dafny ⇒ ( Int ⇒ Result ) = _.pTestCaseExpression
    private val expressionParser    : Dafny ⇒ ( Int ⇒ Result ) = _.pTestExpression
    private val termParser          : Dafny ⇒ ( Int ⇒ Result ) = _.pTestTerm
    private val endLessExprParser   : Dafny ⇒ ( Int ⇒ Result ) = _.pTestEndlessExpr
    private val matchStmtParser     : Dafny ⇒ ( Int ⇒ Result ) = _.pTestMatchStmt
    private val caseStmtParser      : Dafny ⇒ ( Int ⇒ Result ) = _.pTestCaseStmt
    private val methodDeclParser    : Dafny ⇒ ( Int ⇒ Result ) = _.pTestMethodDecl
    //  format: ON

    //  the map from some types to a parser

    //  format: OFF
    private val dispatch = Map(
        classOf[ DafnyProgram ]         → dafnyProgramParser,
        classOf[ TestCalc ]             → calcParser,
        classOf[ TestMatchExpression ]  → matchParser,
        classOf[ TestCaseExpression ]   → caseExprParser,
        classOf[ TestExpression ]       → expressionParser,
        classOf[ TestTerm ]             → termParser,
        classOf[ TestEndlessExpr ]      → endLessExprParser,
        classOf[ TestMatchStmt ]        → matchStmtParser,
        classOf[ TestCaseStmt ]         → caseStmtParser,
        classOf[ TestMethodDecl ]       → methodDeclParser
        
    )
    //  format: ON

    /**
     * Create a parser for terms of type String => Try[T] that parses a String into T.
     *
     * @param T The target type of the prser.
     */
    private def parser[ T <: ASTNode ] : ( Dafny ⇒ ( Int ⇒ xtc.parser.Result ) ) ⇒ ( Source ⇒ Try[ T ] ) = {
        p1 ⇒
            { s ⇒
                val p = new DafnyParser[ T ]( p1 )
                p.makeast( s, p.createConfig( List() ) ) match {
                    case Left( x ) ⇒ Success( x )
                    case Right( m ) ⇒
                        //  m is a vector of messages
                        Failure( new ParseErrorException( s"Parse error ${p.messaging.formatMessages( m )}" ) )
                }
            }
    }
}

trait PredefinedParsers {

    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
    import DafnySyntax._
    import scala.util.Try

    private[ dafnypretty ] val parseAsCalc : Source ⇒ Try[ Calc ] = {
        x ⇒
            val parse = DafnyParser[ Calc ]
            parse( x )
    }

}

/**
 * Convert a String or File source into a Kiama Source for parsing
 */
object Implicits {

    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
    import scala.language.implicitConversions

    /**
     * Make a {@link http://org.bitbucket.inkytonik.kiama.util.Source} from a String
     */
    implicit def stringToSource( s : String ) : Source = StringSource( s )
}
