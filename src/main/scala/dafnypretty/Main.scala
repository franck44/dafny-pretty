/*
 * This file is part of DafnyPP.
 *
 * Copyright (C) 2020 Franck Cassez.
 *
 * DafnyPP is free software: you can  redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * DafnyPP is distributed in the hope that it will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DafnyPP.  (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.dafnypretty

/** Parse general response strings */
object Main extends Formatter {

    override def main( args : Array[ String ] ) : Unit = {

        import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
        import scala.util.{ Success, Failure }
        import DafnySyntax.{ ASTNode, DafnyProgram }
        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

        parse( FileSource( args( 0 ) ) ) match {

            case Success( v ) ⇒
                // println( prettyProduct( v ) )
                logger.info( myPretty( v ) )
                logger.info( prettyProduct( v ) )

            case Failure( f ) ⇒ println( f.getMessage )
        }
    }
}
