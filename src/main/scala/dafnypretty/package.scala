/*
 * This file is part of DafnyPP.
 *
 * Copyright (C) 2020 Franck Cassez.
 *
 * DafnyPP is free software: you can  redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * DafnyPP is distributed in the hope that it will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DafnyPP.  (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44

/**
 * This is the documentation for package [[parser]]
 *
 * ==Overview==
 *
 * This package provides parsers for some non-terminal of the SMTLIB2 grammar (.syntax file).
 *  The parsers are generated from a '.syntax' file using {@link http://sbt-rats}.
 * The AST types of the grammar are collected in [[parser.SMTLIB2Syntax]]
 * ===Usage===
 *
 * The following example
 * illustrates how to create a parser for the SMTLIB2 grammar with axiom  'Command':
 *
 * {{{
 * //  contains the Scala Syntax traits, abstract and case classes
 * scala> import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
 * import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
 *
 * //  contains the SMTLIB2Parser factory
 * scala> import org.bitbucket.franck44.scalasmt.parser._
 * import org.bitbucket.franck44.scalasmt.parser._
 *
 * //   create a parser ot type Source = Try[Command]
 * scala> val parser = SMTLIB2Parser[Command]
 * parser: org.bitbucket.inkytonik.kiama.util.Source =>
 *              scala.util.Try[org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.Command] = <function1>
 * }}}
 *
 * There are two main 'Source's to parse some input:
 * <ul>
 * <li><strong>strings</strong>: a string 's' can parsed using the Kiama
 * StringSource(s) wrapper</li>
 * <li><strong>files</strong>: a file 'f' can be parsed using the Kiama FileSource(f) wrapper</li>
 * </ul>
 * Implicit conversions from String to StringSource can be
 * enabled by importing the Implicits object:
 *
 * {{{
 * scala> import org.bitbucket.franck44.scalasmt.parser.Implicits._
 * import org.bitbucket.franck44.scalasmt.parser.Implicits._
 *
 * scala> val res = parser("(check-sat)")
 * res: scala.util.Try[org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.Command] = Success(CheckSatCmd())
 *
 * }}}
 *
 *  Some parsers are used for testing the grammar rules. For instance, a 'Idtester'
 * provides a tester for terms ::= identifier rule.
 * {{{
 * scala> val parseId = SMTLIB2Parser[ IdTester ]
 * parseId: String => scala.util.Try[org.bitbucket.franck44.scalasmt.SMTLIB2Syntax.IdTester] = <function1>
 *
 * scala> parseId("x")
 * res0: scala.util.Try[org.bitbucket.franck44.scalasmt.SMTLIB2Syntax.IdTester] =
 *                                  Success(IdTester(SymbolId(SSymbol(x))))
 *
 * scala> parseId("(_ x 2)")
 * res1: scala.util.Try[org.bitbucket.franck44.scalasmt.SMTLIB2Syntax.IdTester] =
 *                                  Success(IdTester(IndexedId(SSymbol(x),NumeralIdx(2),List())))
 * }}}
 *
 */
package object dafnypretty {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    /**
     * Get a new logger whose name is formed from the name of the given
     * class with the suffix appended (default: no suffix).
     */
    def getLogger( clazz : Class[ _ ], suffix : String = "" ) : Logger =
        Logger( LoggerFactory.getLogger( clazz.getName + suffix ) )

}
